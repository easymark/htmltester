/*
lang codes          https://www.science.co.il/language/Locale-codes.php
currency codes      https://www.iban.com/currency-codes
*/

var DEBUG_CONFIG = {
    dataLayer: true,
    shoptet: false,
    prestashop_v_1_7: false,
    shopify: true,
    old_ga: false,
    gtm_dynamic_remarketing: false,
    gtm_enhanced_eshop: false,
    webareal_super_shit: false,
    article_analytics: false,
    owl_data_layer: false
};

function gtmPushAsArguments() {
    dataLayer.push(arguments);
}

if(DEBUG_CONFIG.dataLayer){
    window.dataLayer = window.dataLayer || [];

    // gtm config objects
    gtmPushAsArguments('js', new Date());
    gtmPushAsArguments('config', '123456421313');
}

if (DEBUG_CONFIG.shoptet) {
    window.dataLayer.push(
        {
            shoptet: {
                pageType: 'shit',
                cart: {
                    2317: 2,
                    4710: 3
                },
                order: {
                    orderNo: 'FAKE ID',
                    total: 'FAKE PRICE'
                }
            }
        },
        {ga: 'empty'}
    );
}

if (DEBUG_CONFIG.gtm_dynamic_remarketing || DEBUG_CONFIG.gtm_enhanced_eshop) {
    // global gtm events
    dataLayer.push({event: 'gtm.dom', 'gtm.uniqueEventId': 2});
    dataLayer.push({event: 'gtm.load', 'gtm.uniqueEventId': 5});
}

if (DEBUG_CONFIG.gtm_dynamic_remarketing) {
    window.dataLayer = window.dataLayer || [];
    window.dataLayer.push({
            'gtm.start': 123456789,
            'event': 'gtm.js',
            'gtm.uniqueEventId': 3
        });


    // product detail
    dataLayer.push({
        currency: "CZK",
        ecomm_fbid: "18375",
        ecomm_name: "Mycí květina Junior Calypso žabka",
        ecomm_pagetype: "product",
        ecomm_prodid: "18375",
        ecomm_totalvalue: "33"});

    // product detail (arr item)
    gtmPushAsArguments('event', 'page_view', {
        ecomm_pagetype: 'product',
        ecomm_prodid: 34592212,
        ecomm_totalvalue: 29.99,
        ecomm_category: 'Home & Garden',
        isSaleItem: false
    });

    // purchase (arr item)
    gtmPushAsArguments('event', 'conversion', {
        'send_to': 'AW-123456789/AbC-D_efG-h12_34-567',
        'value': 345.89,
        'currency': 'USD',
        'transaction_id': '12345',
    });

    //dataLayer.push({ecomm_pagetype: "cart", ecomm_totalvalue: 33, currency: "CZK"});
}

if (DEBUG_CONFIG.gtm_enhanced_eshop) {
    window.dataLayer = window.dataLayer || [];

    // product detail
    dataLayer.push({
        'ecommerce': {
            'detail': {
                'actionField': {'list': 'Apparel Gallery'},
                'products': [{
                    'name': 'Triblend Android T-Shirt',
                    'id': '12345',
                    'price': '15.25',
                    'brand': 'Google',
                    'category': 'Apparel',
                    'variant': 'Gray'
                }]
            }
        }
    });

    // purchase
    dataLayer.push({
        'ecommerce': {
            'purchase': {
                'actionField': {
                    'id': 'T12345',                         // Transaction ID. Required for purchases and refunds.
                    'affiliation': 'Online Store',
                    'revenue': '35.43',                     // Total transaction value (incl. tax and shipping)
                    'tax':'4.90',
                    'shipping': '5.99',
                    'coupon': 'SUMMER_SALE'
                },
                'products': [{                            // List of productFieldObjects.
                        'name': 'Triblend Android T-Shirt',     // Name or ID is required.
                        'id': '12345',
                        'price': '15.25',
                        'brand': 'Google',
                        'category': 'Apparel',
                        'variant': 'Gray',
                        'quantity': 1,
                        'coupon': ''                            // Optional fields may be omitted or set to empty string.
                    },
                    {
                        'name': 'Donut Friday Scented T-Shirt',
                        'id': '67890',
                        'price': '33.75',
                        'brand': 'Google',
                        'category': 'Apparel',
                        'variant': 'Black',
                        'quantity': 1
                    }]
            }
        }
    });
}

if (DEBUG_CONFIG.old_ga) {
    window.dataLayer = window.dataLayer || [];

    // purchase (transaction)
    dataLayer.push({
        "transactionId":"153279",
        "transactionAffiliation":"",
        "transactionTotal":27.2727,
        "transactionTax":6,
        "transactionCurrency":"CZK",
        "transactionShipping":159,
        "transactionProducts":[{
            "sku":"18375",
            "name":"Mycí květina Junior Calypso žabka",
            "category":"Mycí žínky pro miminka",
            "categoryid":6871,
            "price":27.2727,"quantity":1
        }]
    });
}

if (DEBUG_CONFIG.webareal_super_shit) {
    gtmPushAsArguments('js', new Date());
    gtmPushAsArguments('config', '123456421313');
    gtmPushAsArguments('event', 'conversion', {currency: 'CZK', send_to: 'AW-123456', transaction_id: '945', value: 831.57});
}

if (DEBUG_CONFIG.article_analytics) {
    dataLayer.push({owl_page_type: 'article', owl_article_id: 123});
}

if(DEBUG_CONFIG.prestashop_v_1_7){
    window.prestashop = {
        cart: {
            products: [
                {id_product: "1"},
                {id_product: "2"},
                {id_product: "3"},
                {id_product: "4"}
                ]
        },
        currency: {iso_code: 'CZK'},
        language: {iso_code: 'cs'}
    };

    window.dataLayer.push({ecomm_pagetype: 'cart', ecomm_prodid: ["123", "245"]});
}

if(DEBUG_CONFIG.owl_data_layer){
    window.dataLayer = window.dataLayer || [];
    window.dataLayer.push({"ID" : 2783, "Price" : 16.58, "PageType" : "Product", "owl_pagetype" : "product", "owl_product_id" : 2783});
    window.dataLayer.push({owl_default_module: 1, owl_placeholder: 'my-placeholder', owl_product_count: 4, owl_theme: 'my theme', owl_theme_key: 'my theme key'});
    window.dataLayer.push({owl_default_module: 1, owl_placeholder: 'my-placeholder-1', owl_product_count: 4, owl_theme: 'my theme', owl_theme_key: 'my theme key'});
    window.dataLayer.push({owl_default_module: 1, owl_placeholder: 'my-placeholder-2', owl_product_count: 4, owl_theme: 'my theme', owl_theme_key: 'my theme key'});
}

if(DEBUG_CONFIG.shopify){
    window.meta = {
        page: {
            pageType: 'product'
        },
        product: {
            id: 123456
        }
    };
    window.Shopify = {
        locale: 'cs',
        currency: {active: 'CZK', rate: '1.0'}
    }
}






