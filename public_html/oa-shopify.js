(function () {
    OwlShopify = function () {
        var CONFIG = {
            OA_SCRIPT: 'https://apiprod.owlcure.cz/js/oa_v3.js'    // ext
            //OA_SCRIPT: 'oa_v2.js'                        // local
        };

        var url = document.location.origin + '' + document.location.pathname;

        function pushToDataLayer(obj) {
            window.dataLayer = window.dataLayer || [];
            window.dataLayer.push(obj);
        }

        function loadOwlAnalytics() {
            var s = document.getElementsByTagName('script')[0];
            var b = document.createElement('script');
            b.type = "text/javascript";
            b.async = true;
            b.src = CONFIG.OA_SCRIPT;
            s.parentNode.insertBefore(b, s);
        }

        function getCurrency() {
            return window.Shopify && window.Shopify.currency && window.Shopify.currency.active || null;
        }

        function getLang() {
            return window.Shopify && window.Shopify.locale || null;
        }

        function loadUserData() {
            var lang = getLang();
            if (lang) {
                pushToDataLayer({owl_web_lang: lang});
            }
            var currency = getCurrency();
            if (currency) {
                pushToDataLayer({owl_web_currency: currency});
            }
        }

        function checkPageType() {
            var pageType, productId;
            var meta = window.meta || null;

            if(meta && meta.page && meta.page.pageType){
                pageType = window.meta.page.pageType;
                if(pageType == 'collection'){
                    pageType = 'category';
                } else if(pageType == 'product'){
                    productId = meta.product && meta.product.id ||  null;
                }
            } else if(url.match(/\/cart$/g)){
                pageType = 'cart';
            }


            var objToDataLayer = {};
            if(pageType){
                objToDataLayer.owl_pagetype = pageType;
            }
            if(productId) {
                objToDataLayer.owl_product_id = productId;
            }

            if(Object.keys(objToDataLayer).length){
                pushToDataLayer(objToDataLayer);
            }
        }


        return {
            loadOwlAnalytics: loadOwlAnalytics,
            loadUserData: loadUserData,
            checkPageType: checkPageType
        };
    };

    var shopify = new OwlShopify();

    function ready(fn) { var d = document; (d.readyState == 'loading') ? d.addEventListener('DOMContentLoaded', fn) : fn(); }
    ready(function () {
        shopify.checkPageType();
        shopify.loadUserData();
        shopify.loadOwlAnalytics();
    });
})();
