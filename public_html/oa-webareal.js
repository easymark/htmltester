(function () {
    OwlWebareal = function () {
        var CONFIG = {
            OA_SCRIPT: 'https://apiprod.owlcure.cz/js/oa_v2.js'    // ext
            //OA_SCRIPT: 'oa_v2.js'                        // local
        };

        var url = document.location.origin + '' + document.location.pathname;

        function loadOwlAnalytics() {
            var s = document.getElementsByTagName('script')[0];
            var b = document.createElement('script');
            b.type = "text/javascript";
            b.async = true;
            b.src = CONFIG.OA_SCRIPT;
            s.parentNode.insertBefore(b, s);
        }

        function createDataLayerIfNotExist() {
            window.dataLayer = window.dataLayer || [];
        }

        function checkProductView() {
            var productEl, productId;
            productEl = document.getElementsByClassName('detail-box-product');
            if (productEl && productEl[0]) {
                productId = productEl[0].getAttribute('data-idn');
            }
            if (productId) {
                window.dataLayer.push({owl_product_id: productId});
            }
        }

        function getObjFromDataLayerIdentifiedByKey(searchingKey) {
            var obj;
            var dataLayer = window.dataLayer;
            if (dataLayer && dataLayer.length) {
                for (var i = 0; i < dataLayer.length; i++) {
                    if (dataLayer[i].length) {
                        for (var j = 0; j < dataLayer[i].length; j++) {
                            if (typeof dataLayer[i][j] == 'object' && Object.keys(dataLayer[i][j])) {
                                obj = dataLayer[i][j];
                                for (var key in obj) {
                                    if (key == searchingKey) {
                                        return obj;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return '-1';
        }

        function checkConversion() {
            var obj = getObjFromDataLayerIdentifiedByKey('transaction_id');
            if(obj != '-1'){
                owlFly('order:purchase', {
                    order_id: obj.transaction_id,
                    price: obj.value
                });
            }
        }

        return {
            createDataLayerIfNotExist: createDataLayerIfNotExist,
            checkProductView: checkProductView,
            checkConversion: checkConversion,
            loadOwlAnalytics: loadOwlAnalytics
        };
    };

    var webareal = new OwlWebareal();
    webareal.createDataLayerIfNotExist();

    function ready(fn) { var d = document; (d.readyState == 'loading') ? d.addEventListener('DOMContentLoaded', fn) : fn(); }
    ready(function () {
        webareal.checkProductView();
        webareal.checkConversion();
        webareal.loadOwlAnalytics();
    });
})();
