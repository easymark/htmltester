(function () {
    OwlExitshop = function () {
        var CONFIG = {
            OA_SCRIPT: 'https://apiprod.owlcure.cz/js/oa_v2.js'    // ext
            //OA_SCRIPT: 'oa_v2.js'                        // local
        };

        var url = document.location.origin + '' + document.location.pathname;

        function loadOwlAnalytics() {
            var s = document.getElementsByTagName('script')[0];
            var b = document.createElement('script');
            b.type = "text/javascript";
            b.async = true;
            b.src = CONFIG.OA_SCRIPT;
            s.parentNode.insertBefore(b, s);
        }

        function pushToDataLayer(obj) {
            window.dataLayer = window.dataLayer || [];
            window.dataLayer.push(obj);
        }

        function getCartOnCartPage() {
            var cart = [];
            var cartItemId;
            (document.querySelectorAll('.cart-products input[data-product-id]') || []).forEach(function(product) {
                cartItemId = parseInt((product.getAttribute('data-product-id') || '0').split('-')[0]);
                if(cartItemId){
                    cart.push(cartItemId);
                }
            });
            return cart;
        }

        function attachAddToCartListeners() {
            var cartBtns = document.querySelectorAll('[data-oaaction="cart"]');
            if(cartBtns && cartBtns.length){
                for(var i = 0; i < cartBtns.length; i++){
                    cartBtns[i].addEventListener('click', function(e, args) {
                        var productId = (this.getAttribute('data-product-id') || '').split('-')[0];

                        var quantityElement = document.querySelector('.product-add-to-shopping-basket-quantity>input[type=number]');
                        var quantity =  quantityElement ? parseInt(quantityElement.value) : 1;

                        var cart = [];
                        var cartProductElements = document.querySelectorAll('#hover-cart-products>.product-row>.product-cancel') || [];
                        cartProductElements.forEach(function(el) {
                            cart.push(el.getAttribute('data-id').split('-')[0]);
                        });

                        window.owlFly('cart:add', {
                            item_id: productId,
                            quantity: quantity,
                            cart: cart
                        });
                    });
                }
            }
        }

        function attachCartRemoveListener() {
            var removeCartBtns = document.querySelectorAll('.product-cancel[data-product-id]');
            if(removeCartBtns && removeCartBtns.length){
                for(var i = 0; i < removeCartBtns.length; i++){
                    removeCartBtns[i].addEventListener('click', function(e, args) {
                        var productId = (this.getAttribute('data-product-id') || '').split('-')[0];
                        window.owlFly('cart:remove', {
                            item_id: productId
                        });
                    });
                }
            }
        }

        function attachCartChangeQtyListener() {
            var increaseQtyCartBtns = document.querySelectorAll('button.quantity_increase[data-product-id]');
            var decreaseQtyCartBtns = document.querySelectorAll('button.quantity_decrease[data-product-id]');
            if(increaseQtyCartBtns && increaseQtyCartBtns.length){
                for(var i = 0; i < increaseQtyCartBtns.length; i++){
                    increaseQtyCartBtns[i].addEventListener('click', function(e, args) {
                        var productId = (this.getAttribute('data-product-id') || '').split('-')[0];
                        var quantity = parseInt((this.parentNode.querySelector('input') || {}).value || "1");

                        window.owlFly('cart:changeqty', {
                            item_id: productId,
                            quantity: quantity + 1
                        });
                    });
                }
            }
            if(decreaseQtyCartBtns && decreaseQtyCartBtns.length){
                for(var i = 0; i < decreaseQtyCartBtns.length; i++){
                    decreaseQtyCartBtns[i].addEventListener('click', function(e, args) {
                        var productId = (this.getAttribute('data-product-id') || '').split('-')[0];
                        var quantity = parseInt((this.parentNode.querySelector('input') || {}).value || "1");

                        window.owlFly('cart:changeqty', {
                            item_id: productId,
                            quantity: quantity - 1,
                        });
                    });
                }
            }
        }

        function checkPageType() {
            var bodyEl = document.querySelector('body');
            var bodyClass = bodyEl.getAttribute('class');
            var pageType = null;
            var objToDataLayer = {};
            if(bodyClass.indexOf('homepage') > -1){
                pageType = 'homepage';
            } else if(bodyClass.indexOf('body-cart') > -1){
                pageType = 'cart';
            } else if(bodyClass.indexOf('body-product') > -1){
                pageType = 'product';
            }

            if(pageType){
                objToDataLayer.owl_pagetype = pageType;

                if(pageType === 'product') {
                    var el, productId;
                    el = document.querySelector('[data-product-id]');
                    if(el){
                        productId = (el.getAttribute('data-product-id') || '0').split('-')[0];

                        if(productId){
                            pushToDataLayer({owl_product_id: productId});
                        }
                    }
                } else if(pageType === 'cart') {
                    var cart = getCartOnCartPage();
                    if(cart && cart.length) {
                        objToDataLayer.owl_cart = cart;
                    }

                    attachCartRemoveListener();
                    attachCartChangeQtyListener();
                }
            }

            if(Object.keys(objToDataLayer).length){
                pushToDataLayer(objToDataLayer);
            }
        }

        return {
            loadOwlAnalytics: loadOwlAnalytics,
            attachAddToCartListeners: attachAddToCartListeners,
            checkPageType: checkPageType
        };
    };

    var exitshop = new OwlExitshop();

    window.addEventListener('oa:script_loaded', function () {
        exitshop.attachAddToCartListeners();
    }, false);

    function ready(fn) { var d = document; (d.readyState == 'loading') ? d.addEventListener('DOMContentLoaded', fn) : fn(); }
    ready(function () {
        exitshop.checkPageType();
        exitshop.loadOwlAnalytics();
    });

})();
