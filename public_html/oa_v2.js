/* version 2.8.1 */
(function () {
    OwlAnalytics = function () {

        //LOCALS
        var $_GET = {};
        var token = null;
        var h = '';             //hash
        var a = '';             //part attribute
        var anonyme = '';       //anonyme hash
        var anonymeHashWasGenerated = false;
        var CHAR_SET = 'abcdefghijklmnopqrstuvwxyz';

        var page = document.location.origin + '' + document.location.pathname;
        var search = (document.location.search ? document.location.search.replace('?', '') : '');

        var conversionSent = false;
        var alreadyProceedVirtualPanels = [];
        var isPreview = false;

        var CONFIG = {
            DEBUG: false,
            SEND: true,
            COOKIE_EXP: 36500,
            WARNINGS: true,
            ERRORS: true,
            PANEL_PLACE_ATTEMPTS: 5,
            PANEL_PLACE_TIMEOUT: 200
        };

        var GA_CONFIG = {
            PANEL_NAME_LIMIT: 64
        };

        var API = {
            PROD_MAIN: 'https://owlcure.eu',
            PROD_REDIS: 'https://apiprod.owlcure.cz',
            DEV_MAIN: 'https://apidev.owlcure.cz',
            DEV_REDIS: 'https://apidev.owlcure.cz'
        };

        // DON'T FORGET USE API.PROD_MAIN on production
        var MAIN_BE = API.DEV_MAIN;

        // DON'T FORGET USE API.PROD_REDIS on production
        var REDIS_BE = API.DEV_REDIS;


        var isTest = localStorage && localStorage.getItem && localStorage.getItem('oa_test');
        isTest = isTest ? parseInt(isTest) : isTest;
        if (isTest) {
            MAIN_BE = API.DEV_MAIN;
            REDIS_BE = API.DEV_REDIS;
        }

        var ENDPOINT = {
            ACTION: 'analytics/action',
            INNER: 'panel/inner',
            INNER_PREVIEW: 'panel/preview_hash',
            VERIFY: 'analytics/verification'
        };

        var CHECK_HEADERS = [
            'utm_source',
            'utm_medium',
            'utm_campaign'
        ];

        var MAPPINGS = {        //client: server
            NOTSENDACTIONS: ['hum'],
            PARAMS: {
                // analytics
                item_id: 'oareference',
                order_id: 'oareference',
                email: 'email',
                price: 'oavalue',
                quantity: 'oavalue',
                url: 'url',
                cart: 'cart',

                // magazine analytics
                article_id: 'oareference',
                rating: 'oavalue',

                // both
                filter: 'oafilter'

            },
            FUNCTIONS: {
                // analytics
                'cart:add': 'cart',
                'cart:remove': 'cart-remove',
                'cart:changeqty': 'cart-changeqty',
                'cart:clear': 'cart-clear',
                'cart:identify': 'cart-identify',
                'order:purchase': 'conversion',
                'newsletter:add': 'newsletter-add',
                'user:identify': 'user-identify',
                'product:search': 'product-search',

                // magazine analytics
                'article:bookmark': 'article-bookmark',
                'article:rate': 'article-rate',
                'article:search': 'article-search',
                'article:avoid': 'article-avoid',
            },
            REDIS_FUNCTIONS: [
                'cart:add',
                'product:search'
            ]
        };

        //DETECTION
        function getOS() {
            var OSName = "Unknown OS";
            if (navigator && navigator.userAgent) {
                if (navigator.userAgent.indexOf("Win") != -1) OSName = "Windows";
                if (navigator.userAgent.indexOf("Mac") != -1) OSName = "MacOS";
                if (navigator.userAgent.indexOf("Linux") != -1) OSName = "Linux";
                if (navigator.userAgent.indexOf("Android") != -1) OSName = "Android";
                if (navigator.userAgent.indexOf("like Mac") != -1) OSName = "iOS";
            }
            return OSName;
        }

        var isMobile = {
            Android: function () {
                return navigator.userAgent.match(/Android/i);
            },
            BlackBerry: function () {
                return navigator.userAgent.match(/BlackBerry/i);
            },
            iOS: function () {
                return navigator.userAgent.match(/iPhone|iPad|iPod/i);
            },
            Opera: function () {
                return navigator.userAgent.match(/Opera Mini/i);
            },
            Windows: function () {
                return navigator.userAgent.match(/IEMobile/i);
            },
            any: function () {
                return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
            }
        };

        //HELPERS
        function mergeObjects() {
            if (!arguments || !arguments.length) {
                return {};
            }
            var result = {};

            for (var i = 0; i < arguments.length; i++) {
                for (var arg in arguments[i]) {
                    result[arg] = arguments[i][arg];
                }
            }
            return result;
        }

        function getProp(object, keys, defaultVal) {
            keys = Array.isArray(keys) ? keys : keys.split('.');
            object = object[keys[0]];
            if (object && keys.length > 1) {
                return getProp(object, keys.slice(1), defaultVal);
            }
            return object === undefined ? defaultVal : object;
        }

        function isNullOrUndefined(value) {
            return value === undefined || value == null;
        }

        function allParentNodesVisible(node) {
            while (node.tagName != 'BODY' && node.parentNode) {
                if (window.getComputedStyle(node, null).display == 'none') {
                    return false;
                }
                node = node.parentNode;
            }
            return true;
        }

        //COOKIES
        function deleteCookie(cname) {
            setCookie(cname, '', 0);
        }

        function setCookie(cname, cvalue, exdays) {
            var d = new Date();
            d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
            var expires = "expires=" + d.toUTCString();
            document.cookie = cname + "=" + encodeURIComponent(cvalue) + ";" + expires + ";path=/";
        }

        function getCookie(cname) {
            var name = cname + "=";
            var decodedCookie = decodeURIComponent(document.cookie);
            var ca = decodedCookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) == 0) {
                    return c.substring(name.length, c.length);
                }
            }
            return "";
        }

        function addUserIdentificators(req) {
            req = req || {};

            //add url
            req.url = page;
            req.url_parameters = search;

            //add hashes
            if (h) {
                req.hash = h;
            }
            if (anonyme) {
                req.anonyme = anonyme;
            }
            if (token) {
                req.t = token;
            }
        }

        function addWebUserIdentification(req) {
            var webUserId = getWebUserId();
            if (webUserId) {
                req.web_user_id = webUserId;
            }
        }

        function addProductIdentification(req) {
            var productId = getProductId();
            if (productId != '-1') {
                req.product_id = productId;
            }
        }

        function addArticleIdentification(req) {
            var articleId = getArticleId();
            if (articleId != '-1') {
                req.article_id = articleId;
            }
        }

        function addPanelIdentification(req, panel) {
            req.oareference = panel.id;
            req.automat_id = panel.automat_id;
            req.automat_node_id = panel.automat_node_id;
        }

        function addPageTypeIdentification(req) {
            var pageType = getPageType();
            if (pageType) {
                req.page_type = pageType;
            }
        }

        function addCartItemsIdentification(req) {
            var cart = getCart();
            if (cart) {
                req.cart = cart;
            }
        }

        function addLanguage(req) {
            var lang = getWebLang();
            if (lang) {
                req.lang = lang;
            }
        }

        function addCurrency(req) {
            var currency = getWebCurrency();
            if (currency) {
                req.currency = currency;
            }
        }

        function addWindowSize(req) {
            if (window.screen && window.screen.width) {
                req.width = window.screen.width;
            }
            if (window.screen && window.screen.height) {
                req.height = window.screen.height;
            }
        }

        function addPlaceholders(req) {
            var placeholders = getAllDataLayerObjectsWithKey('owl_placeholder');
            if (placeholders && placeholders.length) {
                req.owl_placeholders = placeholders;
            }
        }

        function getPanelCategoryForGA(panel) {
            return 'Owlcure panel ' + panel.id + ' - ' + panel.name.substring(0, GA_CONFIG.PANEL_NAME_LIMIT);
        }

        function sendPanelAction(panel, action, params) {
            if (isPreview) {
                return
            }

            var data = {
                action: 'panel-' + action
            };

            addPanelIdentification(data, panel);
            addUserIdentificators(data);
            post(MAIN_BE, ENDPOINT.ACTION, data);
            sendGaEvent(getPanelCategoryForGA(panel), action, params);
        }

        function sendGaEvent(eventCategory, eventAction, eventParams) {
            eventParams = eventParams || {};

            if (window.gtag) {
                var data = {
                    event_category: eventCategory
                };

                if (eventParams.nonInteraction) {
                    data.non_interaction = true;
                }

                window.gtag('event', eventAction, data);
            } else if (window.ga) {
                if (Object.keys(eventParams).length) {
                    window.ga('send', 'event', eventCategory, eventAction, 'none', 0, eventParams);
                } else {
                    window.ga('send', 'event', eventCategory, eventAction);
                }
            }
        }

        function isElementInViewport(el, offset) {
            var boundOffset = (offset == null ? 0 : offset);
            var scroll = window.scrollY || window.pageYOffset;
            var boundsTop = el.getBoundingClientRect().top + scroll;

            var viewport = {
                top: scroll,
                bottom: scroll + window.innerHeight,
            };

            var bounds = {
                top: boundsTop + boundOffset,
                bottom: boundsTop + el.clientHeight - boundOffset,
            };

            return (bounds.bottom >= viewport.top && bounds.bottom <= viewport.bottom)
                || (bounds.top <= viewport.bottom && bounds.top >= viewport.top);
        }


        function initInViewportChecker(element, panel) {
            var timeout = null;
            var handler = null;

            if (isElementInViewport(element, 100)) {
                sendPanelAction(panel, 'view', {nonInteraction: true});
            } else {
                handler = function onScroll(event) {
                    if (timeout) {
                        clearTimeout(timeout);
                        timeout = null;
                    }

                    timeout = setTimeout(function () {
                        if (isElementInViewport(element, 100)) {
                            sendPanelAction(panel, 'view', {nonInteraction: true});
                            window.removeEventListener('scroll', handler, false);
                        }
                    }, 100);
                };
                window.addEventListener('scroll', handler, false);
            }
        }

        function callFunctions(jsEvents) {
            for (var i = 0; i < jsEvents.length; i++) {
                processFunctionCall(jsEvents[i].f, jsEvents[i].params);
            }
        }

        function processFunctionCall(fn, args) {
            if (getProp(window, fn)) {
                var argsArr = args && args.length && args.map(function (a) {
                    return a.value
                }) || null;
                window[fn].apply(null, argsArr);
            }
        }

        function executeCode(code) {
            eval(code);
        }

        function createPanels(panels) {
            var body = document.getElementsByTagName('body')[0];
            for (var i = 0; i < panels.length; i++) {
                processPanel(body, panels[i]);
            }
        }

        function getInputsValues(allInputs) {
            var oneInput;
            var inputName;
            var inputType;
            var userAttributes = [];

            for (var j = 0; j < allInputs.length; j++) {
                oneInput = allInputs[j];
                inputName = oneInput.getAttribute('name');
                inputType = oneInput.getAttribute('type');

                switch (inputType) {
                    case 'text':
                    case 'email':
                        userAttributes.push({
                            name: inputName,
                            type: inputType,
                            value: oneInput.value
                        });
                        break;
                    case 'checkbox':
                    case 'radio':
                        if (oneInput.checked) {
                            userAttributes.push({
                                name: inputName,
                                type: inputType,
                                value: oneInput.value
                            });
                        }
                        break;
                }
            }

            return userAttributes;
        }

        function insertAfter(newElement, targetElement) {
            // target is what you want it to go after. Look for this elements parent.
            var parent = targetElement.parentNode;

            // if the parents lastchild is the targetElement...
            if (parent.lastChild == targetElement) {
                // add the newElement after the target element.
                parent.appendChild(newElement);
            } else {
                // else the target has siblings, insert the new element between the target and it's next sibling.
                parent.insertBefore(newElement, targetElement.nextSibling);
            }
        }

        function insertBefore(newElement, targetElement) {
            targetElement.parentNode.insertBefore(newElement, targetElement);
        }

        function processPanel(body, panel) {
            var delay = 0;
            if (panel.config && panel.config.displayDelay == 'On') {
                delay = panel.config.displayDelayParams && panel.config.displayDelayParams.value || 0;
            }
            delay *= 1000;

            // create element
            var element = document.createElement('div');
            var PANEL_CONTAINER_MARKER = 'oa-panel-container-id';
            var isPanelAddedToDOM = false;
            element.setAttribute(PANEL_CONTAINER_MARKER, panel.id);
            element.style = panel.effect.start;

            // show element at start position
            setTimeout(function () {
                element.innerHTML = panel.html;
                switch (panel.panel_type_name) {
                    case 'bar':
                        body.insertBefore(element, body.firstChild);
                        isPanelAddedToDOM = true;
                        break;
                    case 'popup':
                        body.insertBefore(element, body.lastChild);
                        isPanelAddedToDOM = true;
                        break;
                    case 'inner':
                        var htmlMarkerEl = document.querySelector(panel.config.htmlMarker);
                        var insertCondition = true;
                        if (htmlMarkerEl && panel.config.check_visibility) {
                            insertCondition = allParentNodesVisible(htmlMarkerEl);
                        }

                        if (htmlMarkerEl && insertCondition) {
                            var innerPosition = panel.config.innerPosition || 'Replace';

                            switch (innerPosition) {
                                case 'Replace':
                                    htmlMarkerEl.innerHTML = element.innerHTML;
                                    element = htmlMarkerEl;
                                    break;
                                case 'Before':
                                    insertBefore(element, htmlMarkerEl);
                                    break;
                                case 'After':
                                    insertAfter(element, htmlMarkerEl);
                                    break;
                            }

                            //set reference to htmlMarker for clickableElements
                            isPanelAddedToDOM = true;
                        }
                        break;
                    case 'virtual':
                        if (alreadyProceedVirtualPanels.indexOf(panel.id) > -1) {
                            return;
                        }
                        var htmlMarker = document.querySelector(panel.config.htmlMarker);
                        if (htmlMarker) {

                            var domElement, selectorArr, selector, attr, attrValue;
                            var replace = panel.config.replace;
                            for (var key in replace) {
                                selectorArr = key.split('@');
                                selector = selectorArr[0];
                                attr = selectorArr[1];
                                attrValue = replace[key];
                                domElement = selector === '$self' ? htmlMarker : htmlMarker.querySelector(selector);
                                domElement.setAttribute(attr, attrValue);
                            }

                            element = htmlMarker;
                            isPanelAddedToDOM = true;
                        }

                        // remember that this panel was proceed on this page
                        alreadyProceedVirtualPanels.push(panel.id);
                        break;
                }

                // show element on end position (if defined)
                var end = panel.effect.end;
                if (end) {
                    setTimeout(function () {
                        for (var styleAtt in end) {
                            element.style[styleAtt] = end[styleAtt];
                        }
                    }, 100);
                }
                if (isPanelAddedToDOM) {
                    sendPanelAction(panel, 'show', {nonInteraction: true});

                    if (panel.config.javascriptAfterDisplay) {
                        executeCode(panel.config.javascriptAfterDisplay);
                    }

                    if (panel.panel_type_name == 'inner') {
                        initInViewportChecker(element, panel);
                    }
                } else if (panel.config.multiple_attempt) {
                    // repeat try for find inner container (f.e. async cart)
                    panel.attempt = panel.attempt || CONFIG.PANEL_PLACE_ATTEMPTS;
                    panel.attempt -= 1;
                    if (panel.attempt > 0) {
                        setTimeout(function () {
                            processPanel(body, panel);
                        }, CONFIG.PANEL_PLACE_TIMEOUT);
                    }
                    return;
                }

                // set submit event
                if (element.getAttribute('oa-panel-event') === 'submit') {
                    element.addEventListener('submit', function (evt) {
                        var preventDefault = panel.config && panel.config.preventDefault || false;

                        var clickableAction = this.getAttribute('oa-panel-action');
                        var allInputs = element.querySelectorAll('[oa-panel-input]');

                        var actionData = {
                            action: 'panel-' + clickableAction
                        };

                        addPanelIdentification(actionData, panel);

                        if (clickableAction == 'submit') {
                            actionData.form_data = getInputsValues(allInputs);

                            addUserIdentificators(actionData);
                            post(MAIN_BE, ENDPOINT.ACTION, actionData);
                            sendGaEvent(getPanelCategoryForGA(panel), clickableAction, null);
                        }

                        if (preventDefault) {
                            evt.preventDefault();
                            return false;
                        } else {
                            return true;
                        }
                    });
                }

                // set click events
                var clickableElements = element.querySelectorAll('[oa-panel-event="click"]');
                for (var i = 0; i < clickableElements.length; i++) {
                    clickableElements[i].addEventListener('click', function (e, args) {

                        // all
                        var clickableAction = this.getAttribute('oa-panel-action');
                        var allInputs = element.querySelectorAll('[oa-panel-input]');

                        // after click actions
                        var removeElementAfterAction = false;

                        var afterClickActions = this.getAttribute('oa-panel-after-click');
                        if (afterClickActions) {
                            if (afterClickActions.indexOf('remove') > -1) {
                                removeElementAfterAction = true;
                            }
                        }

                        // inner attributes
                        var productId = this.getAttribute('oa-product-id');

                        // oa-validators
                        var oneInput;
                        var inputName;
                        var inputType;
                        var inputValidator;
                        var validationErrors = {};

                        var actionData = {
                            action: 'panel-' + clickableAction
                        };
                        addPanelIdentification(actionData, panel);

                        if (!isNullOrUndefined(productId)) {
                            actionData.product_id = productId;
                        }

                        if (clickableAction == 'submit') {
                            // check validators
                            for (var j = 0; j < allInputs.length; j++) {
                                oneInput = allInputs[j];
                                inputName = oneInput.getAttribute('name');
                                inputType = oneInput.getAttribute('type');
                                inputValidator = oneInput.getAttribute('oa-validators');

                                var validation = validateInput(oneInput, inputName, inputType, inputValidator);
                                if (validation) {
                                    validationErrors[inputName] = validation;
                                }
                            }

                            clearValidationErrors(element);
                            if (Object.keys(validationErrors).length) {
                                // show validation errors
                                displayValidationErrors(element, validationErrors, panel.form_setting.validation_messages);
                                return
                            }

                            actionData.form_data = getInputsValues(allInputs);
                        }

                        addUserIdentificators(actionData);

                        post(MAIN_BE, ENDPOINT.ACTION, actionData);
                        sendGaEvent(getPanelCategoryForGA(panel), clickableAction, null);

                        // prevent href to redirect immediately (wait 100ms for request)
                        if (this.href) {
                            var href = this.href;
                            setTimeout(function () {
                                window.location.href = href;
                            }, 100);
                            e.preventDefault();
                        }

                        if (removeElementAfterAction) {
                            element.remove();
                        }
                    });
                }
            }, delay);
        }

        function validateEmail(email) {
            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        }

        function validateInput(input, name, type, validators) {
            var result = [];
            if (type == 'email') {
                if (!validateEmail(input.value)) {
                    result.push('EMAIL');
                }
            }

            if (validators) {
                if (validators.indexOf('REQUIRED') > -1) {
                    if (type == 'checkbox' && !input.checked) {
                        result.push('REQUIRED');
                    } else if (!input.value) {
                        result.push('REQUIRED');
                    }
                }
            }

            return result.length ? result : null;
        }

        function displayValidationErrors(element, errors, validationMessages) {
            var errorMessageContainer;
            var errorMessages;
            var errorElement;

            for (var err in errors) {

                errorMessageContainer = element.querySelectorAll('[oa-validation-error-' + err + ']');
                errorMessages = document.createElement('div');
                for (var i = 0; i < errors[err].length; i++) {
                    errorElement = document.createElement('div');
                    errorElement.innerText = validationMessages[errors[err][i]];
                    errorMessages.appendChild(errorElement);
                }

                for (var i = 0; i < errorMessageContainer.length; i++) {
                    errorMessageContainer[i].innerHTML = errorMessages.innerHTML;
                    errorMessageContainer[i].style.display = 'block';
                }

            }
        }

        function clearValidationErrors(el) {
            var validationMessages = el.querySelectorAll('[oa-validation-error]');
            for (var i = 0; i < validationMessages.length; i++) {
                validationMessages[i].style.display = 'none';
            }
        }

        // HTTP
        var sendHttp = function (api, endpoint, method, data) {
            var dataForSend = JSON.stringify(data);
            var async = true;
            var request = new XMLHttpRequest();

            request.onload = function () {
                var status = request.status;
                var data = null;
                try {
                    data = JSON.parse(request.responseText);
                } catch (e) {
                }

                if (CONFIG.DEBUG) {
                    console.log('Response from server:');
                    console.log(status);
                    console.log(data);
                    console.log('---------------------------------------------------');
                }

                // Create dynamic panels
                if (data) {
                    if (data.panels && data.panels.length) {
                        createPanels(data.panels);
                    }
                    if (data.js_events && data.js_events.length) {
                        callFunctions(data.js_events);
                    }
                }
            };

            if (CONFIG.DEBUG) {
                console.log('Send request:');
                console.log('url: ' + (api + '/' + endpoint));
                console.log('method: ' + method);
                console.log('data: ' + dataForSend);
                console.log('---------------------------------------------------');
            }

            if (CONFIG.SEND) {
                request.open(method, api + '/' + endpoint, async);
                request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");
                request.setRequestHeader("Accept", "application/json");
                request.send(dataForSend);
            }
        };

        var post = function (api, endpoint, data) {
            sendHttp(api, endpoint, 'POST', data);
        };

        var get = function (api, endpoint, data) {
            sendHttp(api, endpoint, 'GET', data);
        };

        //HASH
        var generateHash = function () {
            var now = new Date().getTime().toString();
            var base = "";
            var uid;

            for (var i = now.length; i < 16; i++) {
                base += CHAR_SET[Math.floor(Math.random() * CHAR_SET.length)] || 'X';
            }
            uid = r() + r() + r() + base + "" + now + r() + r();
            anonymeHashWasGenerated = true;

            return uid;
        };

        var tryIdentifyUser = function () {
            if (h) {          //user was identified
                return;
            } else if (anonyme) {   //identify user by anonyme hash
                deleteCookie('oa_anonyme_hash');
                setCookie('oa_hash', anonyme, CONFIG.COOKIE_EXP);
            }
        };

        //GET PARAMETERS
        function getParameters() {
            document.location.search.replace(/\??(?:([^=]+)=([^&]*)&?)/g, function () {
                function decode(s) {
                    return decodeURIComponent(s.split("+").join(" "));
                }

                $_GET[decode(arguments[1])] = decode(arguments[2]);
            });
        }

        function getAllDataLayerObjectsWithKey(key) {
            var dataObjects = [];
            if (window.dataLayer && window.dataLayer.length) {
                for (var i = 0; i < window.dataLayer.length; i++) {
                    if (window.dataLayer[i] && window.dataLayer[i][key]) {
                        dataObjects.push(window.dataLayer[i]);
                    }
                }
            }
            return dataObjects;
        }

        function getDataLayerObj(key, subkey) {
            var dataObj;
            if (window.dataLayer && window.dataLayer.length) {
                for (var i = 0; i < window.dataLayer.length; i++) {
                    dataObj = window.dataLayer[i];
                    if (dataObj && dataObj[key]) {
                        if (subkey) {
                            if (dataObj[key][subkey]) {
                                return dataObj;
                            }
                        } else {
                            return dataObj;
                        }
                    }
                }
            }
            return null;
        }

        function getDataLayerObjFromArgumentArray(first, second, thirdKey) {
            var dataObj;
            if (window.dataLayer && window.dataLayer.length) {
                for (var i = 0; i < window.dataLayer.length; i++) {
                    dataObj = window.dataLayer[i];
                    if (dataObj && dataObj.length >= 3 && dataObj[0] === first && dataObj[1] === second && dataObj[2].hasOwnProperty(thirdKey)) {
                        return dataObj[2];
                    }
                }
            }
            return null;
        }

        function getDataLayerArrItem(key, subkey) {
            var dataObj;
            if (window.dataLayer && window.dataLayer.length) {
                for (var i = 0; i < window.dataLayer.length; i++) {
                    dataObj = window.dataLayer[i];
                    if (dataObj.length > 2) {
                        if (dataObj[0] == key && dataObj[1] == subkey && typeof dataObj[2] == 'object') {
                            return dataObj[2];
                        }
                    }
                }
            }
            return null;
        }

        function getCart() {
            var key;
            var obj;

            // by Owl data layer
            key = 'owl_cart';
            obj = getDataLayerObj(key);
            if (obj) {
                return obj[key];
            }

            // by GTM remarketing
            key = 'ecomm_pagetype';
            obj = getDataLayerObj(key);
            if (obj && obj['ecomm_pagetype'] === 'cart' && obj['ecomm_prodid']) {
                return obj['ecomm_prodid'];
            }

            return null;
        }

        function getWebUserId() {
            var key;
            var obj;

            // by Owl data layer
            key = 'owl_web_user_id';
            obj = getDataLayerObj(key);
            if (obj) {
                return obj[key];
            }

            return null;
        }

        function getWebLang() {
            var key;
            var obj;

            // by Owl data layer
            key = 'owl_web_lang';
            obj = getDataLayerObj(key);
            if (obj) {
                return obj[key];
            }

            return null;
        }

        function getWebCurrency() {
            var key;
            var obj;

            // by Owl data layer
            key = 'owl_web_currency';
            obj = getDataLayerObj(key);
            if (obj) {
                return obj[key];
            }

            return null;
        }

        function getPageType() {
            var key;
            var obj;

            // by Owl data layer
            key = 'owl_pagetype';
            obj = getDataLayerObj(key);
            if (obj) {
                return obj[key];
            }

            // by GTM remarketing
            key = 'ecomm_pagetype';
            obj = getDataLayerObj(key);
            if (obj) {
                return obj[key];
            }

            return null;
        }

        function getArticleId() {
            var key;
            var obj;

            // by Owl data layer
            key = 'owl_article_id';
            obj = getDataLayerObj(key);
            if (obj) {
                return obj[key];
            }

            return '-1';
        }

        function getProductId() {
            var key;
            var subkey;
            var obj;

            // by Owl data layer
            key = 'owl_product_id';
            obj = getDataLayerObj(key);
            if (obj) {
                return obj[key];
            }

            // by enhanced GTM for e-shops
            key = 'ecommerce';
            subkey = 'detail';
            obj = getDataLayerObj(key, subkey);
            if (obj &&
                !obj[key].event
                && obj[key][subkey].products
                && obj[key][subkey].products[0]
                && obj[key][subkey].products[0].id) {
                return obj[key][subkey].products[0].id;
            }

            // by GTM remarketing
            key = 'ecomm_prodid';
            obj = getDataLayerObj(key);

            if (obj && obj.ecomm_pagetype == 'product') {
                return obj[key];
            }

            // by GTM remarketing (array item)
            obj = getDataLayerArrItem('event', 'page_view');
            if (obj && obj.ecomm_pagetype == 'product' && obj.ecomm_prodid) {
                return obj.ecomm_prodid;
            }

            return '-1';
        }

        function checkConversion() {
            var key;
            var subkey;
            var obj;

            // by enhanced GTM for e-shops
            key = 'ecommerce';
            subkey = 'purchase';
            obj = getDataLayerObj(key, subkey);
            if (obj && obj[key][subkey].actionField) {
                var price = 0;
                try {
                    price = parseInt(obj[key][subkey].actionField.revenue || 0);
                } catch (e) {
                }

                fly('order:purchase', {
                    order_id: obj[key][subkey].actionField.id,
                    price: price
                });
                return;
            }

            // by GTM remarketing
            key = 'purchase';
            obj = getDataLayerObj(key);
            if (obj) {
                var price = 0;
                try {
                    price = parseInt(obj[key].ecomm_totalvalue || 0);
                } catch (e) {
                }

                fly('order:purchase', {
                    order_id: 'no id',
                    price: price
                });
                return;
            }

            // by GTM remarketing (array item)
            obj = getDataLayerArrItem('event', 'conversion');
            if (obj) {
                var price = 0;
                try {
                    price = parseInt(obj.value || 0);
                } catch (e) {
                }

                fly('order:purchase', {
                    order_id: obj.transaction_id,
                    price: price
                });
                return;
            }

            // by old GA
            key = 'transactionId';
            obj = getDataLayerObj(key);
            if (obj) {
                var price = 0;
                try {
                    price = parseInt(obj.transactionTotal);
                } catch (e) {
                }

                fly('order:purchase', {
                    order_id: obj[key],
                    price: price
                });
                return;
            }

            // by arguments array [e_type, e_name, e_object]
            key = 'transaction_id';
            obj = getDataLayerObjFromArgumentArray('event', 'purchase', key);
            if (obj) {
                var price = 0;
                try {
                    price = obj.value;
                } catch (e) {
                }

                fly('order:purchase', {
                    order_id: obj[key],
                    price: price
                });
                return;
            }
        }

        function r() {
            return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
        }

        var printWarning = function (msg) {
            if (CONFIG.WARNINGS) {
                console.log('Owlcure Analytics warning. ' + msg + '. Please visit documentation page http://www.owlcure.com/analytics for proper working.');
            }
        };

        var printError = function (e) {
            if (CONFIG.ERRORS) {
                console.log('Owlcure Analytics error.');
                console.log(e);
                console.log('Please visit documentation page http://www.owlcure.com/analytics for proper working.');
            }
        };

        function findHash() {
            if ($_GET["h"]) {
                updateOaHash($_GET["h"]);
            } else if (getCookie('oa_hash')) {
                updateOaHash(getCookie('oa_hash'));
            }
        }

        function updateOaHash(hash) {
            h = hash;
            setCookie('oa_hash', h, CONFIG.COOKIE_EXP);
        }

        function updateOaAnonymeHash(anonymeHash) {
            anonyme = anonymeHash;
            setCookie('oa_anonyme_hash', anonyme, CONFIG.COOKIE_EXP);
        }

        function updateHash(hash) {
            if (getCookie('oa_hash')) {
                updateOaHash(hash);
                if (getCookie('oa_anonyme_hash')) {
                    deleteCookie('oa_anonyme_hash');
                }
            } else if (getCookie('oa_anonyme_hash')) {
                updateOaAnonymeHash(hash);
            }
        }

        function findAnonymeHash() {
            anonyme = getCookie('oa_anonyme_hash');
        }

        function findPartAtt() {
            if ($_GET["a"]) {
                a = $_GET["a"];
            }
        }

        function sendVisit() {
            var refer = document.referrer || 'direct';
            var data = {
                action: 'visit',
                url: page,
                url_parameters: search,
                refer: "" + refer + "",
                os: getOS(),
                t: token
            };

            // check headers
            for (var i = 0; i < CHECK_HEADERS.length; i++) {
                if (($_GET[CHECK_HEADERS[i]])) {
                    data[CHECK_HEADERS[i]] = ($_GET[CHECK_HEADERS[i]]);
                }
            }

            if (h) {   // by hash
                data.hash = h;
                if (a) {
                    data.a = a;     //add part attribute
                }
                if (anonyme) {
                    data.anonyme = anonyme; //add anonyme hash
                    deleteCookie('oa_anonyme_hash');
                }
            } else {    // by anonyme hash
                if (anonyme) {    // anonyme exists
                    data.anonyme = anonyme;
                } else {        // create anonyme
                    updateOaAnonymeHash(generateHash());
                    data.anonyme = anonyme;
                }
            }

            // add window sizes
            addWindowSize(data);

            // try find client id
            addWebUserIdentification(data);

            // try find product id
            addProductIdentification(data);

            // try find article id
            addArticleIdentification(data);

            // try find page type
            addPageTypeIdentification(data);

            // try find cart
            addCartItemsIdentification(data);

            // try find language
            addLanguage(data);

            // try find currency
            addCurrency(data);

            post(MAIN_BE, ENDPOINT.ACTION, data);
        }

        //GET INNERS
        function getInners() {
            var data = {};

            if (anonymeHashWasGenerated) {
                data.hash_generated = true;
            }

            addUserIdentificators(data);

            // add window sizes
            addWindowSize(data);

            // try find client id
            addWebUserIdentification(data);

            // try find product id
            addProductIdentification(data);

            // try find article id
            addArticleIdentification(data);

            // try find page type
            addPageTypeIdentification(data);

            // try find cart
            addCartItemsIdentification(data);

            // try find language
            addLanguage(data);

            // try find currency
            addCurrency(data);

            // try find placeholders (inner)
            addPlaceholders(data);

            var endpoint = isPreview ? ENDPOINT.INNER_PREVIEW : ENDPOINT.INNER;
            post(REDIS_BE, endpoint, data);
        }

        //PARSE PARAMETERS
        function parseArgument(arg) {
            var obj = {};
            if (arg[0]) {
                obj.action = arg[0];
            }
            if (arg[1]) {
                obj.params = arg[1];
            }

            if (obj.action === 'hum') {
                token = obj.params || 'NO TOKEN';
            }
            return obj;
        }

        function parseQuery(oaArgsArray) {
            if (!oaArgsArray || !oaArgsArray.length) {
                return;
            }
            for (var i = 0; i < oaArgsArray.length; i++) {
                var flyObj = parseArgument(oaArgsArray[i]);
                sendFlyObj(flyObj.action, flyObj.params, MAIN_BE, ENDPOINT.ACTION);
            }
        }

        function fly() {
            var flyObj = parseArgument(arguments);
            if (MAPPINGS.REDIS_FUNCTIONS.indexOf(flyObj.action) > -1) {
                var endpoint = isPreview ? ENDPOINT.INNER_PREVIEW : ENDPOINT.INNER;
                sendFlyObj(flyObj.action, flyObj.params, REDIS_BE, endpoint);
            }
            sendFlyObj(flyObj.action, flyObj.params, MAIN_BE, ENDPOINT.ACTION);
        }

        //CREATE MAIN FNC
        function sendFlyObj(action, params, api, endpoint) {
            try {
                if (MAPPINGS.NOTSENDACTIONS.indexOf(action) > -1) {
                    return;
                }
                if (!action) {
                    printWarning('Missing variable ACTION in function FLY');
                    return;
                }

                if (!MAPPINGS.FUNCTIONS.hasOwnProperty(action)) {
                    printWarning('ACTION ' + action + ' is not allowed');
                    return;
                }

                //block conversion duplication (child script and core script do the same)
                if (action == 'order:purchase') {
                    if (conversionSent) {
                        return;
                    }
                    conversionSent = true;
                }

                //create fly data object
                var fly_body = {
                    action: MAPPINGS.FUNCTIONS[action]
                };

                //check parameters
                if (params) {
                    if (typeof params !== 'object') {
                        printWarning('Bad event parameters ' + params);
                        return;
                    }
                    for (var key in params) {
                        if (MAPPINGS.PARAMS[key]) {
                            fly_body[MAPPINGS.PARAMS[key]] = params[key];  //apply mapping
                        } else {
                            printWarning('attribute ' + key + ' is invalid.');
                        }
                    }
                }

                //add url
                fly_body.url = params && params.url || page;
                fly_body.url_parameters = search;

                //add hashes
                if (h) {
                    fly_body.hash = h;
                }
                if (anonyme) {
                    fly_body.anonyme = anonyme;
                }
                if (token) {
                    fly_body.t = token;
                }

                if (action == 'newsletter-add' || action == 'user-identify') {
                    tryIdentifyUser();
                }

                // try find language
                addLanguage(fly_body);

                // try find currency
                addCurrency(fly_body);

                // add window sizes
                addWindowSize(fly_body);

                post(api, endpoint, fly_body);

            } catch (e) {
                printError(e);
            }

        }

        function owlDispatchEvent(name) {
            try {
                window.dispatchEvent(new Event(name));
                if (window.parent && window.parent.postMessage) {
                    window.parent.postMessage(name, '*');
                }
            } catch (e) {
                // IE
                var event = document.createEvent('Event');
                event.initEvent(name, true, true);
                window.dispatchEvent(event);
            }
        }

        function verifyAnalytics() {
            var verify = $_GET && $_GET['oa_verification_token'] || false;
            if (verify) {
                post(REDIS_BE, ENDPOINT.VERIFY, {t: token});
                setTimeout(function () {
                    window.close();
                }, 1000);
            }
        }

        return {
            init: function () {
                getParameters();

                var isPreview = $_GET && $_GET['oa_panel_preview'] ? true : false;


                findHash();
                findAnonymeHash();

                findPartAtt();

                var query = window.owlFly && window.owlFly.query;
                if (query) {
                    parseQuery(query);
                }

                verifyAnalytics();

                if (!isPreview) {
                    sendVisit();
                    checkConversion();
                }

                getInners();

                window.owlFly = fly;
                window.owlUpdateHash = updateHash;
                owlDispatchEvent('oa:script_loaded');
            },
            setCookie: setCookie,
            getCookie: getCookie,
            mergeObjects: mergeObjects,
            getDataLayerObj: getDataLayerObj
        };
    };

    var owlAnalytics = new OwlAnalytics();

    function ready(fn) {
        var d = document;
        (d.readyState == 'loading') ? d.addEventListener('DOMContentLoaded', fn) : fn();
    }

    ready(function () {
        owlAnalytics.init();
    });
})();
