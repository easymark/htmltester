// Neptun carousel
var el = $('.owl-filter-tophome .prod-filter-inner');
if(el.hasClass('owl-loaded')){
    el.trigger('destroy.owl.carousel');
    el.find('.owl-stage-outer').children().unwrap();
    el.removeClass('owl-center owl-loaded owl-text-select-on');
    el.owlCarousel({
        loop: true,
        responsive: {
            0: { items: 1},
            464:{ items: 2,slideBy:2},
            750:{ items: 3,slideBy:2},
            974:{ items: 3,slideBy:2},
            1170:{ items: 4,slideBy:2}
        },
        dots: false,
        nav: true,
        margin:20
    });
}

// Rodinne baleni carousel - product recommendation
var el = $('#primary_accessories.itembox .itembox-content .owl-carousel');
if (el.hasClass('owl-loaded')) {
    el.trigger('destroy.owl.carousel');
    el.find('.owl-stage-outer').children().unwrap();
    el.removeClass('owl-center owl-loaded owl-text-select-on');

    // responsivity
    var items = 5;
    var nav = el.find(".itembox-item").length > 5 ? true : false;
    if ($(this).width() <= 480) {
        items = 1;
        nav = el.find(".itembox-item").length > 1 ? true : false;
    } else if ($(this).width() <= 768) {
        items = 3;
        nav = el.find(".itembox-item").length > 3 ? true : false;
    }

    el.on("initialized.owl.carousel", function (e) {
        $('#primary_accessories.itembox .owl-item .itembox-item').css({'border-left': '1px solid #e5e5e5'});
        $('#primary_accessories.itembox .owl-item.active .itembox-item').first().css({'border-left': '0px solid #e5e5e5'});
    });

    el.owlCarousel({
        autoplay: false,
        loop: false,
        dots: false,
        items: items,
        nav: nav
    });
}

// Rodinne baleni carousel - popup cart recommendation
var el = $('.added_to_cart_popup .owlcure-products.owl-carousel');
el.on("initialized.owl.carousel", function (e) {
    $('#primary_accessories.itembox .owl-item .itembox-item').css({'border-left': '1px solid #e5e5e5'});
    $('#primary_accessories.itembox .owl-item.active .itembox-item').first().css({'border-left': '0px solid #e5e5e5'});
});

el.owlCarousel({
    responsive: {
        0: {items: 1},
        464: {items: 2, slideBy: 2},
        750: {items: 3, slideBy: 2},
        974: {items: 3, slideBy: 2},
        1170: {items: 4, slideBy: 2}
    },
    autoplay: false,
    loop: false,
    dots: false,
    nav: true
});

