<USER>

    <!--  Base attributes -->
    <USER_ID>12345</USER_ID>
    <CREATED_AT>2019-03-05 20:38:53.86</CREATED_AT>
    <NAME>Jack Sparrow</NAME>
    <EMAIL>js@owlcure.com</EMAIL>
    <!-- ./Base attributes -->

    <!-- New attributes -->
    <RATING></RATING>

    <!-- ./New attributes -->

    <!-- 1 way -->
    <CUSTOM_PARAMETERS>

        <BIRTHDAYS>
            <ITEM>
                <DAY>2019-03-05 20:38:53.86</DAY>
                <NAME>Azor</NAME>
            </ITEM>
            <ITEM>
                <DAY>2020-03-05 20:38:53.86</DAY>
                <NAME>Alík</NAME>
            </ITEM>
        </BIRTHDAYS>

        <PRICE_LEVELS>
            <ITEM>
                <LEVEL>S</LEVEL>
                <PRICE>90%</PRICE>
            </ITEM>
            <ITEM>
                <LEVEL>M</LEVEL>
                <PRICE>80%</PRICE>
            </ITEM>
            <ITEM>
                <LEVEL>L</LEVEL>
                <PRICE>70%</PRICE>
            </ITEM>
        </PRICE_LEVELS>

    </CUSTOM_PARAMETERS>
    <!-- ./1 way -->

    <!-- 2 way -->
    <CUSTOM_PARAMETERS>

        <CUSTOM_ITEM>
            <TYPE>Birthday</TYPE>
            <DAY>2019-03-05 20:38:53.86</DAY>
            <NAME>Azor</NAME>
        </CUSTOM_ITEM>
        <CUSTOM_ITEM>
            <TYPE>Birthday</TYPE>
            <DAY>2020-03-05 20:38:53.86</DAY>
            <NAME>Alík</NAME>
        </CUSTOM_ITEM>

        <CUSTOM_ITEM>
            <TYPE>Price level</TYPE>
            <LEVEL>S</LEVEL>
            <NAME>90%</NAME>
        </CUSTOM_ITEM>
        <CUSTOM_ITEM>
            <TYPE>Price level</TYPE>
            <LEVEL>M</LEVEL>
            <NAME>80%</NAME>
        </CUSTOM_ITEM>
        <CUSTOM_ITEM>
            <TYPE>Price level</TYPE>
            <LEVEL>L</LEVEL>
            <NAME>70%</NAME>
        </CUSTOM_ITEM>

    </CUSTOM_PARAMETERS>
    <!-- ./2 way -->

    <!-- 3 way -->
    <CUSTOM_PARAMETERS>

        <CUSTOM_ITEM>
            <TYPE>Birthday</TYPE>
            <FIRST_VALUE>2019-03-05 20:38:53.86</FIRST_VALUE>
            <SECOND_VALUE>Azor</SECOND_VALUE>
        </CUSTOM_ITEM>
        <CUSTOM_ITEM>
            <TYPE>Birthday</TYPE>
            <FIRST_VALUE>2020-03-05 20:38:53.86</FIRST_VALUE>
            <SECOND_VALUE>Alík</SECOND_VALUE>
        </CUSTOM_ITEM>

        <CUSTOM_ITEM>
            <TYPE>Price level</TYPE>
            <FIRST_VALUE>S</FIRST_VALUE>
            <SECOND_VALUE>90%</SECOND_VALUE>
        </CUSTOM_ITEM>
        <CUSTOM_ITEM>
            <TYPE>Price level</TYPE>
            <FIRST_VALUE>M</FIRST_VALUE>
            <SECOND_VALUE>80%</SECOND_VALUE>
        </CUSTOM_ITEM>
        <CUSTOM_ITEM>
            <TYPE>Price level</TYPE>
            <FIRST_VALUE>L</FIRST_VALUE>
            <SECOND_VALUE>70%</SECOND_VALUE>
        </CUSTOM_ITEM>

    </CUSTOM_PARAMETERS>
    <!-- ./3 way -->


</USER>
