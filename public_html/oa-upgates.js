(function () {
    OwlUpgates = function () {
        var CONFIG = {
            OA_SCRIPT: 'https://apiprod.owlcure.cz/js/oa_v2.js'    // ext
            //OA_SCRIPT: 'oa_v2.js'                        // local
        };

        function loadOwlAnalytics() {
            var s = document.getElementsByTagName('script')[0];
            var b = document.createElement('script');
            b.type = "text/javascript";
            b.async = true;
            b.src = CONFIG.OA_SCRIPT;
            s.parentNode.insertBefore(b, s);
        }

        function pushToDataLayer(obj) {
            window.dataLayer = window.dataLayer || [];
            window.dataLayer.push(obj);
        }

        function getCurrency() {
            return window.upgates && window.upgates.currency || null;
        }

        function getLang() {
            return window.upgates && window.upgates.language || null;
        }

        function loadUserData() {
            var lang = getLang();
            if(lang){
                pushToDataLayer({owl_web_lang: lang});
            }
            var currency = getCurrency();
            if(currency){
                pushToDataLayer({owl_web_currency: currency});
            }
        }

        function attachAddToCartListeners(attempt) {
            if(attempt === 5) {
                return;
            }

            if(window.upgates && window.upgates.on) {
                window.upgates.on('cart.add', function(data) {
                    if(data && data.id) {
                        var formRows = document.querySelectorAll('#snippet--options .form-row');
                        var filter = [];
                        if(formRows.length) {
                            for(var i = 0; i < formRows.length; i++){
                                var dimension = (formRows[i].querySelector('label') || {}).innerText;
                                var value = (formRows[i].querySelector('select>option[selected]') || {}).innerText;
                                if(dimension && value) {
                                    filter.push({
                                        dimension: dimension,
                                        value: value
                                    });
                                }
                            }
                        }
                        window.owlFly('cart:add', {
                            item_id: data.id,
                            quantity: data.quantity,
                            filter: filter
                        });
                    }
                });
            } else {
                setTimeout(function() {
                    attachAddToCartListeners(attempt + 1);
                }, 1000);
            }
        }

        return {
            loadUserData: loadUserData,
            loadOwlAnalytics: loadOwlAnalytics,
            attachAddToCartListeners: attachAddToCartListeners
        };
    };

    var upgates = new OwlUpgates();

    function ready(fn) { var d = document; (d.readyState == 'loading') ? d.addEventListener('DOMContentLoaded', fn) : fn(); }
    ready(function () {
        upgates.loadUserData();
        upgates.attachAddToCartListeners(0);
        upgates.loadOwlAnalytics();
    });
})();
