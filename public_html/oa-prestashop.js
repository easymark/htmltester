(function () {
    OwlPrestashop = function () {
        var CONFIG = {
            OA_SCRIPT: 'https://apiprod.owlcure.cz/js/oa_v2.js'    // ext
            //OA_SCRIPT: 'oa_v2.js'                        // local
        };

        var url = document.location.origin + '' + document.location.pathname;
        var prestashopVersion = null;

        function determineVersion() {
            if(window.prestashop) {
                prestashopVersion = '1.7';
            } else {
                prestashopVersion = '1.6';
            }
        }

        function pushToDataLayer(obj) {
            window.dataLayer = window.dataLayer || [];
            window.dataLayer.push(obj);
        }

        function loadOwlAnalytics() {
            var s = document.getElementsByTagName('script')[0];
            var b = document.createElement('script');
            b.type = "text/javascript";
            b.async = true;
            b.src = CONFIG.OA_SCRIPT;
            s.parentNode.insertBefore(b, s);
        }

        function getCurrency() {
            var currency;

            switch(prestashopVersion){
                case '1.7': currency = window.prestashop && window.prestashop.currency && window.prestashop.currency.iso_code || null;
                    break;
                case '1.6': currency = null;
                    break;
            }

            return currency;
        }

        function getLang() {
            var lang;

            switch(prestashopVersion){
                case '1.7': lang = window.prestashop && window.prestashop.language && window.prestashop.language.iso_code || null;
                    break;
                case '1.6': lang = null;
                    break;
            }
            return lang;
        }

        function loadUserData() {
            var lang = getLang();
            if(lang){
                pushToDataLayer({owl_web_lang: lang});
            }
            var currency = getCurrency();
            if(currency){
                pushToDataLayer({owl_web_currency: currency});
            }
        }

        function attachAddToCartListeners() {
            switch(prestashopVersion){
                case '1.7':
                    window.prestashop && window.prestashop.on && window.prestashop.on(
                        'updateCart',
                        function(e, args) {
                            if(e && e.reason && e.reason.linkAction && e.reason.linkAction == 'add-to-cart'){
                                if(e && e.resp){
                                    var productId = e.resp.id_product;
                                    var cart = e.resp.cart
                                        && e.resp.cart.products
                                        && e.resp.cart.products.length
                                        && e.resp.cart.products.map(function(p) {
                                            return p.id_product || null;
                                        }) || [];
                                    window.owlFly('cart:add', {
                                        item_id: productId,
                                        quantity: 1,
                                        cart: cart
                                    });
                                }

                            }
                        });
                    break;
                case '1.6':
                    var cartBtn = document.querySelector('.ajax_add_to_cart_button') || document.querySelector('#add_to_cart>button');

                    if(cartBtn){
                        cartBtn.addEventListener('click', function(e, args) {
                            var productIdElement = document.querySelector('#product_page_product_id');
                            var productId =  productIdElement ? productIdElement.value : null;

                            var quantityElement = document.querySelector('#quantity_wanted');
                            var quantity =  quantityElement ? parseInt(quantityElement.value) : null;

                            var cart = [];
                            var cartProductElements = document.querySelectorAll('.cart_block_list .products [data-id]') || [];
                            cartProductElements.forEach(function(el) {
                                cart.push(el.getAttribute('data-id').replace('cart_block_product_', '').split('_')[0]);
                            });

                            window.owlFly('cart:add', {
                                item_id: productId,
                                quantity: quantity,
                                cart: cart
                            });

                        });
                    }

                    break;
            }

        }

        return {
            determineVersion: determineVersion,
            attachAddToCartListeners: attachAddToCartListeners,
            loadUserData: loadUserData,
            loadOwlAnalytics: loadOwlAnalytics
        };
    };

    var prestashop = new OwlPrestashop();

    setTimeout(function() {
        prestashop.determineVersion();

        window.addEventListener('oa:script_loaded', function () {
            prestashop.attachAddToCartListeners();
        }, false);

        prestashop.loadUserData();
        prestashop.loadOwlAnalytics();
    }, 0);
})();
