(function () {
    OwlShoptet = function () {
        var CONFIG = {
            OA_SCRIPT: 'https://apiprod.owlcure.cz/js/oa_v2.js'    // ext
            //OA_SCRIPT: 'oa_v2.js'                        // local
        };

        var url = document.location.origin + '' + document.location.pathname;

        function loadOwlAnalytics() {
            var s = document.getElementsByTagName('script')[0];
            var b = document.createElement('script');
            b.type = "text/javascript";
            b.async = true;
            b.src = CONFIG.OA_SCRIPT;
            s.parentNode.insertBefore(b, s);
        }

        function getShoptetObj() {
            return window.dataLayer && window.dataLayer[0] && window.dataLayer[0].shoptet;
        }

        function checkConversion() {
            var shoptetDataObj = getShoptetObj();
            if (!shoptetDataObj || !shoptetDataObj.order) {
                return;
            }

            var conversion = {
                orderNo: shoptetDataObj.order.orderNo,
                total: shoptetDataObj.order.total
            };

            if (conversion.orderNo && conversion.total) {
                owlFly('order:purchase', {
                    order_id: conversion.orderNo,
                    price: conversion.total
                });
            }
        }

        function checkCart() {
            var shoptetDataObj = getShoptetObj();
            if (!shoptetDataObj || !shoptetDataObj.cart) {
                return;
            }

            var oa = new OwlAnalytics();
            var actualCart = shoptetDataObj.cart;
            var sentCart = oa.getCookie('oa_last_cart');
            var previousUrl = oa.getCookie('oa_previous_url') || url;

            if (JSON.stringify(actualCart) != sentCart) {
                if (sentCart) {
                    sentCart = JSON.parse(sentCart);
                } else {
                    sentCart = {};
                }
                var actualCartItemsCount = Object.keys(actualCart).length;
                var sentCartItemsCount = Object.keys(sentCart).length;
                if (actualCartItemsCount == 0 && sentCartItemsCount != 0) {
                    owlFly('cart:clear', {url: previousUrl});
                } else {
                    var added = [];
                    var removed = [];
                    var changeQty = [];
                    var mergedCart = oa.mergeObjects(sentCart, actualCart);

                    for (var id in mergedCart) {
                        if (!sentCart.hasOwnProperty(id) && actualCart.hasOwnProperty(id)) {
                            added.push({id: id, qty: actualCart[id]});
                        } else if (sentCart.hasOwnProperty(id) && !actualCart.hasOwnProperty(id)) {
                            removed.push(id);
                        } else if (sentCart[id] != actualCart[id]) {
                            changeQty.push({id: id, qty: actualCart[id]});
                        }
                    }

                    for (var i = 0; i < added.length; i++) {
                        owlFly('cart:add', {item_id: added[i].id, quantity: added[i].qty, url: previousUrl});
                    }

                    for (var i = 0; i < removed.length; i++) {
                        owlFly('cart:remove', {item_id: removed[i], url: previousUrl});
                    }

                    for (var i = 0; i < changeQty.length; i++) {
                        owlFly('cart:changeqty', {item_id: changeQty[i].id, quantity: changeQty[i].qty, url: previousUrl});
                    }
                }
            }
            oa.setCookie('oa_last_cart', JSON.stringify(actualCart), 30);
            oa.setCookie('oa_previous_url', url, 1);
        }

        return {
            loadOwlAnalytics: loadOwlAnalytics,
            checkConversion: checkConversion,
            checkCart: checkCart
        };
    };

    var shoptet = new OwlShoptet();

    window.addEventListener('oa:script_loaded', function () {
        shoptet.checkCart();
    }, false);

    shoptet.loadOwlAnalytics();
    shoptet.checkConversion();
})();


